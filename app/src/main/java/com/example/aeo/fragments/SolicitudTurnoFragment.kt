package com.example.aeo.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide

import com.example.aeo.R
import com.example.aeomovil.model.response.TurnoResponse
import com.example.aeomovil.task.APIService
import kotlinx.android.synthetic.main.fragment_solicitud_turno.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDateTime

/**
 * A simple [Fragment] subclass.
 */
class SolicitudTurnoFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_solicitud_turno, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        lyIncorrecto.visibility = View.GONE
        lyCorrecto.visibility = View.GONE
        //lyBotom.visibility = View.GONE
        val idUser = "2";
        //
        searchTurno(idUser)


        btnConfirmarTurno.setOnClickListener {
            val fech = LocalDateTime.now();


            val dateToISOString = fech.toString() + 'Z';
            println(dateToISOString)


            //FORMATTER.format(fech);

            Log.d("TAG", "Fecha---- "+ fech)
            //Log.d("TAG", "Fecha---- "+ date)

            //validarTurno(fech)

        }


    }

    private fun searchTurno(id: String){
        doAsync {
            val call = getRetrofit().create(APIService::class.java).solicitudTurno(id).execute()
            val response = call.body() as TurnoResponse

            uiThread {
                Log.e("Tag", ""+call)

                if(response.estatus == "true"){
                    Log.e("Tag", "No puedes solictar turno ")
                    lyIncorrecto.visibility = View.VISIBLE
                    lyCorrecto.visibility = View.GONE
                    lyBotom.visibility = View.GONE

                    tvMensajeReponse.text = getString(R.string.title_carta_asignada)
                    tvTituloUno.text = getText(R.string.fecha_registro)
                    tvContenidoUno.text = response.datosCI.fechaRegistro

                    tvTituloDos.text = getText(R.string.folio_registro_viaje)
                    tvContenidoDos.text = response.datosCI.folioViaje

                    tvTituloTres.text = getText(R.string.folio_registro_carta)
                    tvContenidoTres.text = response.datosCI.estatusCarta

                    val requestManager = context?.let { it1 -> Glide.with(it1) }
                    val requestBuilder = requestManager?.load(R.drawable.incorrecto)
                    if (requestBuilder != null) {
                        requestBuilder.into(ivEstatus)
                    }

                }else{
                    Log.e("Tag", "Puedes solictar turno ")
                    lyIncorrecto.visibility = View.GONE
                    lyCorrecto.visibility = View.VISIBLE
                    lyBotom.visibility = View.VISIBLE

                    tvMensajeReponseC.text = getString(R.string.title_carta_solicitar)
                    tvContenidoUnoC.setText(response.mensaje)

                    val requestManager = context?.let { it1 -> Glide.with(it1) }
                    val requestBuilder = requestManager?.load(R.drawable.correcto)
                    if (requestBuilder != null) {
                        requestBuilder.into(ivEstatusC)
                    }


                    /*val requestManager = Glide.with(this@TurnoActivity)
                    val requestBuilder = requestManager.load(R.drawable.correcto)
                    requestBuilder.into(ivEstatus)*/
                }
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}
