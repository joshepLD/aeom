package com.example.aeo.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.aeo.R
import com.example.aeo.activities.MainActivity
import com.example.aeo.adapter.CartaInstruccionAdapter
import com.example.aeomovil.model.response.CartaInstruccionesResponse
import com.example.aeomovil.task.APIService
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * A simple [Fragment] subclass.
 */
class CartaInstruccionesFragment : Fragment() {

    private lateinit var linearLAyoutv:LinearLayoutManager
    private lateinit var recyClerView :RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_carta_instrucciones, container, false)
        recyClerView = view.findViewById(R.id.rvCartaI)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        Log.e("Tag", "Hola")
        searchCI()
    }

    private fun searchCI(){
        Log.e("Tag", "Hola search CI")
        doAsync {
            val call = getRetrofit().create(APIService::class.java).cartaInstrucciones("1").execute()
            val response = call.body() as CartaInstruccionesResponse
            uiThread {
                linearLAyoutv = LinearLayoutManager(context)
                recyClerView.layoutManager = linearLAyoutv
                recyClerView.adapter = CartaInstruccionAdapter(context as MainActivity,response.data)
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}
