package com.example.aeo.utilities

enum class MessageError {
    VALIDA_RED,
    DATOS_INVALDO,
    CONTRASEÑA_INCORECTA,
    DISPOSITIVO_NO_REGISTRADO,
    VALIDA_CAMPOS
}