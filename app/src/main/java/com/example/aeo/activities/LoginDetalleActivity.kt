package com.example.aeo.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.aeo.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginDetalleActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_detalle)
    }

    private fun enableUIElements(enable: Boolean){
        app_bar.setExpanded(!enable)
    }

}