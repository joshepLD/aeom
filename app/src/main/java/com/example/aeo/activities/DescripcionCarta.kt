package com.example.aeo.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.aeo.R
import kotlinx.android.synthetic.main.activity_descripcion_carta.*


class DescripcionCarta : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_descripcion_carta)

        //
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Carta de Instrucciones"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
        //

        var tvFolio = findViewById<TextView>(R.id.tvFolioD)
        var tvTipoServicio = findViewById<TextView>(R.id.tvTipoServicioD)
        var tvTipoViaje = findViewById<TextView>(R.id.tvTipoViajeD)
        var tvCliente = findViewById<TextView>(R.id.tvClienteD)
        var tvNumeroViaje = findViewById<TextView>(R.id.tvNumeroViajeD)
        var tvCartaPorte = findViewById<TextView>(R.id.tvCartaPorteD)
        var tvNombreContacto = findViewById<TextView>(R.id.tvNombreContactoD)
        var tvNumeroContacto = findViewById<TextView>(R.id.tvNumeroContactoD)
        var tvHorarioInicio = findViewById<TextView>(R.id.tvHorarioInicioD)
        var tvHorarioCarga = findViewById<TextView>(R.id.tvHorarioCargaD)
        var tvHorarioDescarga = findViewById<TextView>(R.id.tvHorarioDescargaD)

        var folio: String = intent.getStringExtra("folio")
        var tipoServicio: String = intent.getStringExtra("tipo_servicio")
        var tipoViaje: String = intent.getStringExtra("tipo_viaje")
        var cliente: String = intent.getStringExtra("cliente")
        var numeroViaje: String = intent.getStringExtra("numero_viaje")
        var cartaPorte: String = intent.getStringExtra("carta_porte")
        var nombreContaco: String = intent.getStringExtra("nombre_contacto")
        var numeroContacto: String = intent.getStringExtra("tel_contacto")
        var horarioInicio: String = intent.getStringExtra("horario_inicio")
        var horarioCarga: String = intent.getStringExtra("horario_carga")
        var horarioDescarga: String = intent.getStringExtra("horario_descarga")

        tvFolio.text = folio
        tvTipoServicio.text = tipoServicio
        tvTipoViaje.text = tipoViaje
        tvCliente.text = cliente
        tvNumeroViaje.text = numeroViaje
        tvCartaPorte.text = cartaPorte
        tvNombreContacto.text = nombreContaco
        tvNumeroContacto.text = numeroContacto
        tvHorarioInicio.text = horarioInicio
        tvHorarioCarga.text = horarioCarga
        tvHorarioDescarga.text = horarioDescarga

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
