package com.example.aeo.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.example.aeo.R
import com.example.aeo.fragments.CartaInstruccionesFragment
import com.example.aeo.fragments.HomeFragment
import com.example.aeo.fragments.SolicitudTurnoFragment
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var toolbar: Toolbar
    lateinit var toolbarText: TextView
    lateinit var drawerLayout: DrawerLayout
    lateinit var navViewAdministracion: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)


        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, 0, 0)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navViewAdministracion = findViewById(R.id.nav_administrador)
        navViewAdministracion.setNavigationItemSelectedListener(this)

        if(null == savedInstanceState) {
            val blankFragment = HomeFragment()
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, blankFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
        }

        Log.e("Tag", "onCreate")
        //setBottomMenu()
        //onToolbarName()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        //Modulo  de administrador
        Log.e("Tag", "onNavigation")
        when (item.itemId) {
            R.id.nav_inicio -> {
                val homeFragment = HomeFragment()
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, homeFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                supportFragmentManager.executePendingTransactions()
                //bottom_navigation.selectedItemId=R.id.action_home
                //onToolbarName()

            }
            R.id.nav_ayuda -> {
                Log.e("Tag", "item Ayuda")
                val solicitudTurnoFragment = SolicitudTurnoFragment()
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, solicitudTurnoFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                supportFragmentManager.executePendingTransactions()
                //bottom_navigation.deselectAllItems()
                //onToolbarName()
            }
            R.id.nav_ci -> {
                val cartaInstruccionesFragment = CartaInstruccionesFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, cartaInstruccionesFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
            }
            /*R.id.nav_sesion -> {
                onBackPressed()
            }*/
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    /*override fun onBackPressed() {
        //super.onBackPressed()
        val alerDialog = AlertDialog.Builder(this)
        alerDialog.setTitle("Alerta")
        alerDialog.setMessage("Cerrar sesión?")
        alerDialog.setPositiveButton("Si"){dialog, which ->
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        alerDialog.setNegativeButton("No"){dialog, which ->
        }
        val dialog = alerDialog.create()
        dialog.show()

    }*/

    /*private fun BottomNavigationView.deselectAllItems() {
        val menu = this.menu
        for(i in 0 until menu.size()) {
            (menu.getItem(i) as? MenuItemImpl)?.let {
                it.isExclusiveCheckable = false
                it.isChecked = false
                it.isExclusiveCheckable = true
            }
        }
    }*/
}
