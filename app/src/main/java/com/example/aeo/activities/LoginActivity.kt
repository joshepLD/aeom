package com.example.aeo.activities

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.aeo.R
import com.example.aeo.utilities.MessageError
import com.example.aeomovil.model.response.LoginResponse
import com.example.aeomovil.task.APIService
import com.example.aeomovil.utilities.Utils
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login_detalle.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class LoginActivity : AppCompatActivity() {

    var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        dialog = SpotsDialog.Builder().setContext(this).build()

        edtContraseña.requestFocus()
        edtUsuario.requestFocus()

        edtContraseña.setOnClickListener {
            enableUIElements(true)

        }
        edtUsuario.setOnClickListener {
            enableUIElements(true)

        }

        /*btnIniciar.setOnClickListener {
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            startActivity(intent)
            /*enableUIElements(true)
            loadingCarga.visibility = VISIBLE
            contentLogin.visibility = GONE
            avi.show()
            searchLogin(edtUsuario.text.toString(), edtContraseña.text.toString())*/
        }*/

        btnIniciar.setOnClickListener {
            if (edtUsuario.text!!.isEmpty() || edtContraseña.text!!.isEmpty()) {
                Utils.messageError(applicationContext, MessageError.VALIDA_CAMPOS)
            } else {
                if (Utils.veryfyAvailableNetwork(this)) {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    searchLogin(edtUsuario.text.toString(), edtContraseña.text.toString())
                } else {
                    Utils.messageError(this, MessageError.VALIDA_RED)
                }
            }

        }

    }

    private fun searchLogin(usr: String, pass: String) {
        dialog?.setMessage("Autenticando..")
        dialog?.setCancelable(false)
        dialog?.apply {
            show()
        }
        doAsync {
            val call =
                getRetrofit().create(APIService::class.java).login(usr, pass, "351872093080841")
                    .execute()
            val response = call.body() as LoginResponse

            Log.e("Tag", "----->" + call)

            uiThread {
                if (response.estatus == "true" && response.smartphone.estatus == "true") {
                    initCharacter(response)
                } else {
                    if (response.estatus == "true" && response.smartphone.estatus == "false") {
                        Utils.messageError(
                            applicationContext,
                            MessageError.DISPOSITIVO_NO_REGISTRADO
                        )
                    } else {
                        if (response.estatus == "false" && response.smartphone.estatus == "true") {
                            Utils.messageError(
                                applicationContext,
                                MessageError.CONTRASEÑA_INCORECTA
                            )
                        } else {
                            Utils.messageError(applicationContext, MessageError.DATOS_INVALDO)
                        }
                    }
                    //Log.e("Tag", "NO")
                    dialog?.dismiss()
                }
            }
        }
    }

    private fun initCharacter(response: LoginResponse) {

        val sharedPreference = getSharedPreferences("mi_app_aeo", Context.MODE_PRIVATE)
        var editor = sharedPreference.edit()

        editor.putString("id_usuario", response.resultado.idUser)
        editor.putString("usuario", response.resultado.name)
        editor.putString("apellidoP", response.resultado.apellidoP)
        editor.putString("apellidoM", response.resultado.apellidoM)
        editor.putString("aperacionU", response.resultado.operacionU)

        //editor.putString("id_smartphone", response.smartphone.datosSmart.idSmart)
        editor.apply()

        dialog?.dismiss()
        val intent = Intent(this@LoginActivity, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun enableUIElements(enable: Boolean) {
        app_bar.setExpanded(!enable)
        activity_Detalle.isNestedScrollingEnabled = !enable

        btnIniciar.visibility = VISIBLE

        //edtLContraseñaD.visibility = VISIBLE

    }

    /*fun addUsuario(usuario:UsuarioEntity){
        doAsync {  }
    }*/
}
