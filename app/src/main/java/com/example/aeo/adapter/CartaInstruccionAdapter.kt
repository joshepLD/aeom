package com.example.aeo.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.aeo.R
import com.example.aeo.activities.*
import com.example.aeo.fragments.CartaInstruccionesFragment
import com.example.aeo.model.models.Data
import kotlinx.android.synthetic.main.item_carta_instrucciones.view.*

class CartaInstruccionAdapter (val contexto:MainActivity, val items:List<Data>):RecyclerView.Adapter<CartaInstruccionAdapter.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(contexto).inflate(R.layout.item_carta_instrucciones, parent,false))
    }

    override fun getItemCount(): Int {
     return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val modelData = items.get(position)

        holder.folio.text = modelData.folio
        holder.tipoServicio.text = modelData.cartaPorte
        holder.cliente.text = modelData.cliente

        holder.descripcion.setOnClickListener {
            val intent = Intent(contexto, DescripcionCarta::class.java)
            intent.putExtra("folio",""+modelData.folio)
            intent.putExtra("tipo_servicio",""+modelData.tipoServicio)
            intent.putExtra("tipo_viaje",""+modelData.tipoViaje)
            intent.putExtra("cliente",""+modelData.cliente)
            intent.putExtra("numero_viaje",""+modelData.numeroViaje)
            intent.putExtra("carta_porte",""+modelData.cartaPorte)
            intent.putExtra("nombre_contacto",""+modelData.nombreContactoCarga)
            intent.putExtra("tel_contacto",""+modelData.telContactoCarga)
            intent.putExtra("horario_inicio",""+modelData.horarioInicio)
            intent.putExtra("horario_carga",""+modelData.horarioCarga)
            intent.putExtra("horario_descarga",""+modelData.horarioDescarga)
            contexto.startActivity(intent)
        }

        holder.mapas.setOnClickListener {
            val intent = Intent(contexto, RutaDosActivity::class.java)
            contexto.startActivity(intent)
        }
    }

    class ViewHolder (view:View):RecyclerView.ViewHolder(view){
        val folio = view.tvFolio
        val tipoServicio = view.tvTipoServicio
        val cliente = view.tvCliente
        val descripcion = view.lyDescripcion
        val mapas = view.lyMapas
    }

}