package com.example.aeomovil.task

import com.example.aeomovil.model.response.CartaInstruccionesResponse
import com.example.aeomovil.model.response.LoginResponse
import com.example.aeomovil.model.response.TurnoResponse
import com.example.aeomovil.model.response.ValidarTurnoResponse
import retrofit2.Call
import retrofit2.http.*
import java.time.LocalDate
import java.time.LocalDateTime

interface APIService {

    @GET("ws_scp.php?funcion=validaUsrSmph")
    fun login(@Query("usr") usuario:String, @Query("psw") password:String, @Query("imei") imei:String) : Call<LoginResponse>

    @GET("ws_scp.php?funcion=validaSolicitudTurnoOperador")
    fun solicitudTurno(@Query("idUsuarioOperador") idUsiario:String) : Call<TurnoResponse>

    @GET("ws_scp.php?funcion=creaRegistroSolicitud")
    fun validarTurno(@Query("fechaSolicitud") fechaSolicitud:LocalDateTime,
                     @Query("idUsuarioOperador") idUsuarioOperador:String,
                     @Query("idEquipo") idEquipo:String) : Call<ValidarTurnoResponse>

    @GET("WSGetCI.php?")
    fun cartaInstrucciones(@Query("id_operador") idOperador:String) : Call<CartaInstruccionesResponse>
}
// http://187.188.108.33:18080/web_services/WSGetCI.php?id_operador=1
// http://187.188.108.33:18080/web_services/ws_scp.php?funcion=validaSolicitudTurnoOperador&idUsuarioOperador=3
// http://187.188.108.33:18080/web_services/
