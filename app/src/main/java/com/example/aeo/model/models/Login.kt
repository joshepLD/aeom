package com.example.aeomovil.model.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Login (@SerializedName("id_usuario") var idUser: String,
             @SerializedName("usuario") var user: String,
             @SerializedName("contasena") var contra: String,
             @SerializedName("nombre_usuario") var name: String,
             @SerializedName("apellido_paterno") var apellidoP: String,
             @SerializedName("apellido_materno") var apellidoM: String,
             @SerializedName("tipo Acceso") var tipoAcceso: String,
             @SerializedName("operacion_usuario") var operacionU: String,
             @SerializedName("clave_operacion") var claveOper: String

)
