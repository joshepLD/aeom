package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class CoordenadasLugarCarga (@SerializedName("Latitud") var latitudCarga: Float,
                             @SerializedName("Longitud") var longitudCarga: Float
)
