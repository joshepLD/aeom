package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class Data (@SerializedName("folio") var folio: String,
            @SerializedName("tipo_servicio")var tipoServicio: String,
            @SerializedName("tipo_viaje")var tipoViaje: String,
            @SerializedName("placa")var placa: String,
            @SerializedName("cliente")var cliente: String,
            @SerializedName("cliente_carga")var clienteCarga: String,
            @SerializedName("cliente_paga")var clientePaga: String,
            @SerializedName("numero_viaje")var numeroViaje: String,
            @SerializedName("carta_porte")var cartaPorte: String,
            @SerializedName("inicio")var inicio: Inicio,
            @SerializedName("lugar_carga")var lugarCarga: LugarCarga,
            @SerializedName("lugar_descarga")var lugarDescarga: LugarDescarga,
            @SerializedName("nombre_contacto_carga")var nombreContactoCarga: String,
            @SerializedName("tel_contacto_carga")var telContactoCarga: String,
            @SerializedName("horario_inicio")var horarioInicio: String,
            @SerializedName("horario_carga")var horarioCarga: String,
            @SerializedName("horario_descarga")var horarioDescarga: String,
            @SerializedName("ruta")var ruta: Ruta,
            @SerializedName("maniobras")var Maniobras: String
)