package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class Ruta (@SerializedName("datos") var datos: String,
            @SerializedName("coordenadas") var coordenadas: ArrayList<CoordenadasRuta>
)