package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class CoordenadasRuta (@SerializedName("Latitud") var latitudRuta: Float,
                       @SerializedName("Longitud") var longitudRuta: Float
)