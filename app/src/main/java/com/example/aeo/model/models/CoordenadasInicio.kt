package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class CoordenadasInicio (@SerializedName("Latitud") var latitudInicio: Float,
                         @SerializedName("Longitud") var longitudInicio: Float
)
