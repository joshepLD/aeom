package com.example.aeomovil.model.response

import com.google.gson.annotations.SerializedName

class ValidarTurnoResponse (@SerializedName("estatus") var estatus : String,
                            @SerializedName("mensaje") var mensaje : String)