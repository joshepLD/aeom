package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class CoordenadasLugarDescarga (@SerializedName("Latitud") var latitudDescarga: Float,
                                @SerializedName("Longitud") var longitudDescarga: Float
)