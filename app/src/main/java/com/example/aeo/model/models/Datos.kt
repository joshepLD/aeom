package com.example.aeomovil.model.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Datos (@SerializedName("evento") var evento: String,
             @SerializedName("fecha_inicio") var fechaI: String,
             @SerializedName("fecha_termino") var fechaT: String)

