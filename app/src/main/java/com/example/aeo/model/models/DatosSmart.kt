package com.example.aeomovil.model.models

import com.google.gson.annotations.SerializedName

class DatosSmart (@SerializedName("id_smartphone") var idSmart: String,
                  @SerializedName("imei") var imei: String,
                  @SerializedName("marca") var marca: String,
                  @SerializedName("modelo") var modelo: String,
                  @SerializedName("descripcion") var descripcion: String)