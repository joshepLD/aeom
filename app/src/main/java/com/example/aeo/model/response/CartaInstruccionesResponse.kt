package com.example.aeomovil.model.response

import com.example.aeo.model.models.Data
import com.example.aeomovil.model.models.DatosCI
import com.google.gson.annotations.SerializedName

class CartaInstruccionesResponse (@SerializedName("type_proccess") var TypeProccess : String,
                                  @SerializedName("carta_instrucciones") var CartaInstrucciones : String,
                                  @SerializedName("data") var data: ArrayList<Data>)


