package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class Inicio (@SerializedName("tipo") var tipo: String,
              @SerializedName("coordenadas") var coordenadas: ArrayList<CoordenadasInicio>
)