package com.example.aeomovil.model.models

import com.google.gson.annotations.SerializedName

class Smartphone (@SerializedName("estatus") var estatus: String,
                  @SerializedName("datos_smartphone") var datosSmart: DatosSmart)