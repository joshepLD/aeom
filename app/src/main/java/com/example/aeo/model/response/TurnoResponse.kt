package com.example.aeomovil.model.response

import com.example.aeomovil.model.models.DatosCI
import com.google.gson.annotations.SerializedName

class TurnoResponse (@SerializedName("estatus") var estatus : String,
                     @SerializedName("datos_CI") var datosCI : DatosCI,
                     @SerializedName("mensaje") var mensaje : String)

